package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//Активити для изменения пароля
public class ChangePasswordActivity extends AppCompatActivity {

    EditText pas1, pas2;
    SharedPreferences sh;
    SharedPreferences.Editor ed;
    Boolean back_presed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //Айдишки
        pas1 = findViewById(R.id.firstpas);
        pas2 = findViewById(R.id.secondpas);

        //Настройка SharedPreferences
        sh = getSharedPreferences("SweetHopes", 0);
        ed = sh.edit();
    }

    //При нажатии на сохранения
    public void ConfirmChange (View v) {
        //Проверка на аустоту полей
        if (!TextUtils.isEmpty(pas1.getText()) && !TextUtils.isEmpty(pas2.getText())) {
            //Проверка на совпадение паролей
            if (String.valueOf(pas1.getText()).equals(String.valueOf(pas2.getText()))) {
                //Проверка на "пробел"
                if (!String.valueOf(pas1.getText()).equals(" ")) {
                    //Загрузка значения
                    ed.putBoolean("ispassword", true);
                    ed.putString("password", String.valueOf(pas1.getText()));
                    ed.commit();

                    Toast.makeText(this, "Успешно!", Toast.LENGTH_SHORT).show();

                    //Переход на активити с введением пароля
                    Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, "Пароль не может быть пробелом!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Пароли не совпадают!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Есть пустые поля!", Toast.LENGTH_SHORT).show();
        }
    }

    //При нажатии на отмену
    public void CancelChange (View v) {
        //Переход на главное активити
        Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //При нажатии на Back (спасибо Артемию)
    @Override
    public void onBackPressed() {
        if(!back_presed){
            back_presed = true;
            Toast.makeText(this, "Нажмите ещё раз что-бы выйти", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    back_presed = false;
                }
            },2000);
        }else{
            System.exit(0);
        }
    }
}
