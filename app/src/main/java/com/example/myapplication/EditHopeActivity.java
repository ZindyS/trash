package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//Класс активити с описанием элемента
public class EditHopeActivity extends AppCompatActivity {

    SharedPreferences sh;
    EditText nameofhope, discr, date, time;
    Spinner status;
    Button cancel, enter;
    Boolean bebey;
    TextView textView, text10, text13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hope);

        //Настройка SharedPreferences
        sh = getSharedPreferences("SweetHopes", 0);

        //Задание айдишек
        nameofhope = findViewById(R.id.editText2);
        discr = findViewById(R.id.editText3);
        date = findViewById(R.id.editText5);
        time = findViewById(R.id.editText4);
        status = findViewById(R.id.spinner2);
        enter = findViewById(R.id.done_editing);
        cancel = findViewById(R.id.cancel_editing);
        textView = findViewById(R.id.noteditable_status);
        text10 = findViewById(R.id.textView10);
        text13 = findViewById(R.id.textView13);

        //Вспомогательная переменная, показывающая возможность редактирования в данный момент
        bebey = false;

        //Вызов кастомного метода
        Type();

        //Задаём значения
        date.setText(sh.getString("Date", ""));
        time.setText(sh.getString("Time", ""));
        textView.setText(sh.getString("Status", ""));
        text10.setText(sh.getString("Name", ""));
        //Если нет/есть описания(ие)
        if (sh.getString("Discr", "").equals("")) {
            text13.setText("*отсутствует*");
            text13.setGravity(Gravity.CENTER);
        } else {
            text13.setText(sh.getString("Discr", ""));
            text13.setGravity(Gravity.LEFT);
        }
    }

    //Кастомный метод
    private void Type () {
        //Проверка на возможность редактирования
        if (!bebey) {
            //Ставим невозможность редактирования
            date.setInputType(InputType.TYPE_NULL);
            time.setInputType(InputType.TYPE_NULL);

            //Настройка видимости
            textView.setVisibility(View.VISIBLE);
            status.setVisibility(View.INVISIBLE);
            text10.setVisibility(View.VISIBLE);
            text13.setVisibility(View.VISIBLE);
            discr.setVisibility(View.INVISIBLE);
            nameofhope.setVisibility(View.INVISIBLE);

            //Убираем текст и фокус (клавиатуру)
            nameofhope.setText("");
            discr.setText("");
            nameofhope.clearFocus();
            discr.clearFocus();
            date.clearFocus();
            time.clearFocus();
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            //Ставим текст
            text10.setText(sh.getString("Name", ""));
            text13.setText(sh.getString("Discr", ""));

            //Если нет/есть описания(ие)
            if (sh.getString("Discr", "").equals("")) {
                text13.setText("*отсутствует*");
                text13.setGravity(Gravity.CENTER);
            } else {
                text13.setText(sh.getString("Discr", ""));
                text13.setGravity(Gravity.LEFT);
            }
        } else {
            //Даём возможность редактирования
            date.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
            time.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);

            //Настройка видимости
            status.setVisibility(View.VISIBLE);
            textView.setVisibility(View.INVISIBLE);
            text10.setVisibility(View.INVISIBLE);
            text13.setVisibility(View.INVISIBLE);
            nameofhope.setVisibility(View.VISIBLE);
            discr.setVisibility(View.VISIBLE);

            //Ставим текст
            discr.setText(sh.getString("Discr", ""));
            nameofhope.setText(sh.getString("Name", ""));
            text10.setText("");
            text13.setText("");
        }
    }

    //При нажатии на кнопку сохранения/редактирования
    public void mChange(View v) {
        //Проверка на возможность редактирования
        if (!bebey) {
            bebey = true;

            //Кастомный метод
            Type();

            //Ставим в кнопки текст
            enter.setText("Сохранить");
            cancel.setText("Отмена");
        } else {
            //Проверка на пустоту полей и кол-во символов в поле даты и времени
            if (!TextUtils.isEmpty(nameofhope.getText()) && !TextUtils.isEmpty(date.getText()) && !TextUtils.isEmpty(time.getText()) && time.length() == 5 && date.length() == 10) {
                //Проверка формата времени
                boolean isNormalTime = true;
                for (int i = 0; i<time.length(); i++) {
                    if (i != 2 && (String.valueOf(time.getText()).toCharArray()[i] == '0' || String.valueOf(time.getText()).toCharArray()[i] == '1' || String.valueOf(time.getText()).toCharArray()[i] == '2' || String.valueOf(time.getText()).toCharArray()[i] == '3' || String.valueOf(time.getText()).toCharArray()[i] == '4' || String.valueOf(time.getText()).toCharArray()[i] == '5' || String.valueOf(time.getText()).toCharArray()[i] == '6' || String.valueOf(time.getText()).toCharArray()[i] == '7' || String.valueOf(time.getText()).toCharArray()[i] == '8' || String.valueOf(time.getText()).toCharArray()[i] == '9')) {
                        continue;
                    } else if (i == 2 && String.valueOf(time.getText()).toCharArray()[i] == ':') {
                        continue;
                    } else {
                        isNormalTime = false;
                        break;
                    }
                }
                if (isNormalTime) {
                    //Проверка формата даты
                    boolean isNormalDate = true;
                    for (int i = 0; i<date.length(); i++) {
                        if ((i != 2 && i != 5) && (String.valueOf(date.getText()).toCharArray()[i] == '0' || String.valueOf(date.getText()).toCharArray()[i] == '1' || String.valueOf(date.getText()).toCharArray()[i] == '2' || String.valueOf(date.getText()).toCharArray()[i] == '3' || String.valueOf(date.getText()).toCharArray()[i] == '4' || String.valueOf(date.getText()).toCharArray()[i] == '5' || String.valueOf(date.getText()).toCharArray()[i] == '6' || String.valueOf(date.getText()).toCharArray()[i] == '7' || String.valueOf(date.getText()).toCharArray()[i] == '8' || String.valueOf(date.getText()).toCharArray()[i] == '9')) {
                            continue;
                        } else if ((i == 2 || i == 5) && String.valueOf(date.getText()).toCharArray()[i] == '.') {
                            continue;
                        } else {
                            isNormalDate = false;
                            break;
                        }
                    }
                    if (isNormalDate) {
                        //Кастомный статичный метод изменения всех значений
                        MainActivity.EditAll(sh.getInt("pos", 0), String.valueOf(nameofhope.getText()), String.valueOf(date.getText()), String.valueOf(time.getText()), String.valueOf(discr.getText()), status.getSelectedItem().toString());

                        //Переход на активность
                        Intent intent = new Intent(EditHopeActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(this, "Проверьте формат даты!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Проверьте формат времени!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "У вас есть пустые или заполненные не по форме поля!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //При нажатии на отмену/назад
    public void mCancel(View v) {
        //Проверка разрешения редактировать
        if (!bebey) {
            //Переход на активность
            Intent intent = new Intent(EditHopeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            bebey = false;

            //Кастомный метод
            Type();

            //Изменение текста в кнопках
            enter.setText("Изменить");
            cancel.setText("Назад");
        }
    }
}
