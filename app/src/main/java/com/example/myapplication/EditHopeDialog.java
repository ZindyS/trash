package com.example.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

//Класс кастомного AlertDialog'а для изменения имени, проверка пароля, фильтрации имени и описания
public class EditHopeDialog extends AppCompatDialogFragment {

    String message = "", st = "";
    EditText bruh;
    int pos = 0;
    String s = "Изменить", ara = "Введите имя желания";
    TextView text;

    //Кастомный конструктор, если нужно изменить имя
    public EditHopeDialog (String stroke, int pos) {
        message = stroke;
        this.pos = pos;
    }

    //Кастомный конструктор, если нужно отфильтровать имя или описание
    public EditHopeDialog (String s, String stroke) {
        this.s = s;
        ara = stroke;
    }

    //Кастомный конструктор, если нужно проверить пароль
    public EditHopeDialog (String s, String stroke, String st) {
        this.s = s;
        ara = stroke;
        this.st = st;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        //Создание главного builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        //Подключение view'шки
        View view = inflater.inflate(R.layout.hope_alertdialog, null);
        builder.setView(view);

        //Айдишки
        bruh = view.findViewById(R.id.editText);
        text = view.findViewById(R.id.textView2);

        //Настройка диалога
        bruh.setText(message);
        text.setText(ara);
        bruh.setHint(ara);

        //Создание "позитивной" кнопки
        builder.setPositiveButton(s, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (s.equals("Изменить")) {
                    //Если нужно изменить
                    //Вызов кастомного статичного метода
                    MainActivity.ChangeHope(String.valueOf(bruh.getText()), pos);
                } else if (ara.equals("Введите имя желания")) {
                    //Если нужно отфильтровать имена
                    //Вызов кастомного статичного метода
                    MainActivity.findHope(String.valueOf(bruh.getText()));
                } else if (ara.equals("Введите старый пароль")) {
                    //Если нужно поменять пароль
                    //Переход на след. активность
                    if (st.equals(String.valueOf(bruh.getText()))) {
                        Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), "Неправильный пароль!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //Если нужно отфильтровать описания
                    //Вызов кастомного статичного метода
                    MainActivity.findDiscr(String.valueOf(bruh.getText()));
                }
            }
        });

        //Создание "негативной" кнопки
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        return builder.create();
    }
}
