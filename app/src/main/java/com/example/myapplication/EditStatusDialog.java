package com.example.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

//Класс кастомного AlertDialog'а для изменения статуса, его фильтрации и для вызова списка фозможных фильтеров
public class EditStatusDialog extends AppCompatDialogFragment {

    int idd = -5, number = -1;
    String[] catNamesArray;
    String title;
    int kek = 3;

    //Кастомный конструктор, если надо изменить статус
    public EditStatusDialog (int idd, String[] fuck, String title) {
        this.idd = idd;
        catNamesArray = fuck;
        this.title = title;
    }

    //Кастомный конструктор, если надо отфильтровать статус или показать список фильтеров
    public EditStatusDialog(String[] fuck, String title, int kek) {
        catNamesArray = fuck;
        this.title = title;
        this.kek = kek;
        number = kek;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Создаём билдер
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                //Добавляем переключатели
                .setSingleChoiceItems(catNamesArray, kek,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                number = item;
                            }
                        })
                //Добавляем "позитивную" кнопку
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Проверка на выбор элемента
                        if (number != -1) {
                            if (idd != -5) {
                                //Если нужно изменить статус
                                MainActivity.ChangeStatus(catNamesArray[number], idd);
                            } else if (title.equals("Выберите фильтр")){
                                //Если нужно показать список фильтеров
                                MainActivity.setFilter(catNamesArray[number]);
                            } else {
                                MainActivity.findFilter(catNamesArray[number]);
                            }
                        } else {
                            Toast.makeText(getContext(), "Нужно выбрать что-нибудь!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                //Добавляем "негативную" кнопку
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }
}
