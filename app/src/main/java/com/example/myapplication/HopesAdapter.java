package com.example.myapplication;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

//Кастомный класс адаптера для RecView
public class HopesAdapter extends RecyclerView.Adapter<HopesAdapter.VH> {

    List<BookModel> list;
    ArrayList<View> root = new ArrayList<View>();
    int pos = 0;

    //Кастомный конструктор
    public HopesAdapter (List<BookModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Создаём новый объект класс VH, а так же записываем все View'шки для нашего меню
        root.add(LayoutInflater.from(parent.getContext()).inflate(R.layout.hope_item, parent, false));
        pos++;
        return new VH(root.get(pos-1));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, final int position) {
        //Задаём текста
        holder.name.setText(list.get(position).getHope());
        holder.status.setText(list.get(position).getStatus());
        holder.date.setText(list.get(position).getDate());
        holder.time.setText(list.get(position).getTime());

        //Проверка на статус и подбор соотв. цвета
        if (list.get(position).getStatus().equals("Выполнено")) {
            holder.status.setTextColor(Color.argb(255, 25, 255, 200));
        } else if (list.get(position).getStatus().equals("Отложено")) {
            holder.status.setTextColor(Color.argb(255, 255, 100, 100));
        } else if (list.get(position).getStatus().equals("Выполняется")) {
            holder.status.setTextColor(Color.argb(255, 0, 0, 0));
        }

        //Обработчик долгого нажатия на элемент
        holder.lay.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Кастомный метод для вызова контекстного меню
                MainActivity.SomeClick(position, root);
                return true;
            }
        });

        //Обработчик нажатия на элемент
        holder.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Кастомный метод для перехода на след. активити
                MainActivity.SetNewActivity(position);
            }
        });
    }

    //Запрашиваем кол-во элементов
    @Override
    public int getItemCount() {
        return list.size();
    }

    //Класс с содержимым элемента
    public class VH extends RecyclerView.ViewHolder {
        TextView status, name, date, time;
        LinearLayout lay;
        public VH(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            name = itemView.findViewById(R.id.name_hope);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            lay = itemView.findViewById(R.id.lay);
        }
    }
}
