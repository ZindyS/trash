package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sh;
    EditText depasword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Настройка SharedPreferences
        sh = getSharedPreferences("SweetHopes", 0);

        //Айдишки
        depasword = findViewById(R.id.depasword);
    }

    //При нажатии на продолжить
    public void onContinue (View v) {
        //Проверка на правильность пароля
        String str = String.valueOf(depasword.getText());
        if (str.equals(sh.getString("password", "123"))) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Неправильный пароль!", Toast.LENGTH_SHORT).show();
        }
    }
}
