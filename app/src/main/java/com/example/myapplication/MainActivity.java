package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//Главный класс с списком желаний
public class MainActivity extends AppCompatActivity {

    SharedPreferences sh;
    static SharedPreferences.Editor ed;
    EditText editHope;
    static BookModel model;
    static List<BookModel> organizationList, someList;
    BottomSheetBehavior bottomSheetBehavior;
    LinearLayout botwat;
    Button generate, sure, cancel;
    static TextView sometext;
    Spinner spin;
    static RecyclerView recyclerView;
    static HopesAdapter adapter;
    View darkBruh;
    static FragmentManager manager;
    static DrawerLayout draw;
    static Context context;
    static PopupMenu popupMenu;
    NavigationView navigationView;
    static int whatChoosed = 3;
    boolean back_presed = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Настройка нижней менюшки
        botwat = findViewById(R.id.botwat);
        darkBruh = findViewById(R.id.dark_Bruh);
        bottomSheetBehavior = BottomSheetBehavior.from(botwat);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        darkBruh.setVisibility(View.INVISIBLE);

        //Задание айдишек
        sh = getSharedPreferences("SweetHopes", 0);
        ed = sh.edit();
        editHope = findViewById(R.id.yourhope);
        generate = findViewById(R.id.generate);
        sure = findViewById(R.id.sure);
        cancel = findViewById(R.id.cancel);
        sometext = findViewById(R.id.bruh);
        spin = findViewById(R.id.spinner);
        recyclerView = findViewById(R.id.recView);
        draw = findViewById(R.id.draw);
        navigationView = findViewById(R.id.nav_view);

        //Задание значений вспомог. переменным
        context = MainActivity.this;
        manager = getSupportFragmentManager();
        model = new BookModel();

        //Записываем значения с таблицы в два list'а
        organizationList = SQLite.select().
                from(BookModel.class).queryList();
        someList = SQLite.select().
                from(BookModel.class).queryList();
        if (!organizationList.isEmpty()) {
            sometext.setVisibility(View.INVISIBLE);
        }

        //При нажатии на элемент Spinner'а (выпадающий список)
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                //Настройка цвета выбранного элемента
                if (pos == 0) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.argb(255, 0, 0, 0));
                } else if (pos == 1) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.argb(255, 255, 100, 100));
                } else {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.argb(255, 25, 255, 200));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) { }
        });

        //Обработка изменений статуса нижней менюшки
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //Проверка на статус
                if(newState == BottomSheetBehavior.STATE_EXPANDED) {
                    //При клике на свободное пространство
                    darkBruh.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                            editHope.clearFocus();
                            darkBruh.setVisibility(View.INVISIBLE);
                        }
                    });
                    //При нажатии на кнопку отмены
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Убираем клавиатуру
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            editHope.clearFocus();

                            //Настройка закрытия нижнего меню
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                            editHope.clearFocus();
                            darkBruh.setVisibility(View.INVISIBLE);
                            editHope.setText("");
                        }
                    });
                    //При нажатии на кнопку согласия
                    sure.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Проверка на пустоту поля
                            if (!TextUtils.isEmpty(editHope.getText())) {
                                //Убираем клавиатуру
                                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                                editHope.clearFocus();

                                //Дата и время
                                Date currentDate = new Date();
                                //Формат даты
                                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                                String dateText = dateFormat.format(currentDate);
                                //Формат времени
                                DateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                                String timeText = timeFormat.format(currentDate);

                                //Запись данных
                                model.set_id(organizationList.size());
                                model.setHope(String.valueOf(editHope.getText()));
                                model.setDate(dateText);
                                model.setTime(timeText);
                                model.setDiscription("");
                                model.setStatus(spin.getSelectedItem().toString());
                                model.save();

                                //Вызов кастомного метода
                                Bruh();

                                //Настройка закрытия нижнего меню
                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                sometext.setVisibility(View.INVISIBLE);
                                editHope.setText("");
                                darkBruh.setVisibility(View.INVISIBLE);
                            } else {
                                Toast.makeText(MainActivity.this, "Поле пустое!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    //При нажатии на кнопку генерации желания
                    generate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainActivity.this, "Пока не работает :/", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    //Убираем клавиатуру
                    editHope.clearFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    //Настройка закрытия нижнего меню
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    sometext.setVisibility(View.INVISIBLE);
                    editHope.setText("");
                    darkBruh.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) { }
        });

        //Обработка нажатий кнопок в левом меню
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    //При нажатии на фильтр
                    case R.id.theme:
                        //Вызов кастомного AlertDialog'а
                        String[] some = {"По имени", "По описанию", "По статусу", "По дате и времени"};
                        String title = "Выберите фильтр";
                        EditStatusDialog statusDialog = new EditStatusDialog(some, title, whatChoosed);
                        statusDialog.show(manager, "statusDialog");
                        break;
                    //При нажатии на подключение соц. сетей
                    case R.id.somesites:
                        Toast.makeText(MainActivity.this, "Вы нажали на Подключить соц. сети", Toast.LENGTH_SHORT).show();
                        break;
                    //При нажатии на поддержку автора
                    case R.id.autor:
                        Toast.makeText(MainActivity.this, "Вы нажали на Поддержать автора", Toast.LENGTH_SHORT).show();
                        break;
                    //При нажатии на изменение пароля
                    case R.id.changepassword:
                        //Вызов кастомного AlertDialog'а
                        if (!sh.getString("password", " ").equals(" ")) {
                            EditHopeDialog hopeDialog = new EditHopeDialog("Продолжить", "Введите старый пароль", sh.getString("password", " "));
                            hopeDialog.show(manager, "hopeDialog");
                        } else {
                            Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        break;
                }
                return false;
            }
        });

        //Вызов кастомного метода
        Bruh();
    }



    //Кастомный метод для обновления значений list'а
    public static void Bruh() {
        sometext.setVisibility(View.INVISIBLE);
        organizationList.clear();
        organizationList = SQLite.select().
                from(BookModel.class).queryList();
        //Проверка на пустоту list'а
        if (!organizationList.isEmpty()) {
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new HopesAdapter(organizationList);
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
            sometext.setVisibility(View.VISIBLE);
        }
    }

    //При нажатии на плюсик
    public void onClicked (View v) {
        //Открытие нижнего меню и затемнение заднего фона
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        darkBruh.setVisibility(View.VISIBLE);
    }

    //При нажатии на стрелочку
    public void onLeftMenuClicked(View v) {
        //Открытие левого меню
        draw.openDrawer(GravityCompat.START);
    }

    //При долгом нажатии на элемент
    public static void SomeClick(final int position, ArrayList<View> v) {
        //Вызов контексного меню
        popupMenu = new PopupMenu(context, v.get(position));
        popupMenu.inflate(R.menu.popup_menu);

        //Обработка нажатий на элементы
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    //Нажатие на изменение имени желания
                    case R.id.edit_hope_menu:
                        //Вызов кастомного AlertDialog'а
                        EditHopeDialog hopeDialog = new EditHopeDialog(organizationList.get(position).getHope(), position);
                        hopeDialog.show(manager, "hopeDialog");
                        return true;
                    //Нажатие на изменение статуса
                    case R.id.edit_status_menu:
                        //Вызов кастомного AlertDialog'а
                        String[] some = {"Выполняется", "Отложено", "Выполнено"};
                        String title = "Выберите статус желания";
                        EditStatusDialog statusDialog = new EditStatusDialog(position, some, title);
                        statusDialog.show(manager, "statusDialog");
                        return true;
                    //Нажатие на удалить
                    case R.id.delete_menu:
                        //Удаление значения из таблицы
                        SQLite.delete(BookModel.class)
                                .where(BookModel_Table.hope.is(String.valueOf(organizationList.get(position).getHope())))
                                .execute();
                        //Вызов кастомного метода
                        Bruh();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    //При изменение имени в кастомном AlertDialog'е
    public static void ChangeHope(String newHope, int position) {
        model.set_id(position);
        model.setStatus(organizationList.get(position).getStatus());
        model.setTime(organizationList.get(position).getTime());
        model.setDate(organizationList.get(position).getDate());
        model.setHope(newHope);
        model.setDiscription(organizationList.get(position).getDiscription());
        model.save();
        //Вызов кастомного метода
        Bruh();
    }

    //При изменение статуса в кастомном AlertDialog'е
    public static void ChangeStatus(String item, int position) {
        model.set_id(position);
        model.setStatus(item);
        model.setTime(organizationList.get(position).getTime());
        model.setHope(organizationList.get(position).getHope());
        model.setDate(organizationList.get(position).getDate());
        model.setDiscription(organizationList.get(position).getDiscription());
        model.save();
        //Вызов кастомного метода
        Bruh();
    }

    //При обычном нажатии на элемент
    public static void SetNewActivity(int position) {
        ed.putInt("pos", position);
        ed.putString("Name", someList.get(position).getHope());
        ed.putString("Discr", someList.get(position).getDiscription());
        ed.putString("Time", someList.get(position).getTime());
        ed.putString("Date", someList.get(position).getDate());
        ed.putString("Status", someList.get(position).getStatus());
        ed.commit();
        //Переход на след. активность
        Intent intent = new Intent(context, EditHopeActivity.class);
        context.startActivity(intent);
    }

    //При нажатии на сохранение в EditHopeActivity
    public static void EditAll(int position, String name, String date, String time, String discr, String status) {
        model.set_id(position);
        model.setStatus(status);
        model.setTime(time);
        model.setHope(name);
        model.setDate(date);
        model.setDiscription(discr);
        model.save();
        //Вызов кастомного метода
        Bruh();
    }

    //Обработка нажатий на элемент кастомного AlertDialog'а с фильтрами
    public static void setFilter(String nameoffilter) {
        //Проверка того, что выбрал пользователь
        if (nameoffilter.equals("По статусу")) {
            whatChoosed = 2;
            //Вызов кастомного AlertDialog'а
            String[] some = {"Выполняется", "Отложено", "Выполнено"};
            String title = "Выберите статус желания";
            EditStatusDialog statusDialog = new EditStatusDialog(some, title, -1);
            statusDialog.show(manager, "statusDialog");
        } else if (nameoffilter.equals("По дате и времени")) {
            whatChoosed = 3;
            //Вызов кастомного метода
            Bruh();
        } else if (nameoffilter.equals("По имени")) {
            whatChoosed = 0;
            //Вызов кастомного AlertDialog'а
            EditHopeDialog hopeDialog = new EditHopeDialog("Поиск", "Введите имя желания");
            hopeDialog.show(manager, "hopeDialog");
        } else if (nameoffilter.equals("По описанию")) {
            whatChoosed = 1;
            //Вызов кастомного AlertDialog'а
            EditHopeDialog hopeDialog = new EditHopeDialog("Поиск", "Введите описание желания");
            hopeDialog.show(manager, "hopeDialog");
        }
    }

    //При нажатии на элемент фильтра статусов
    public static void findFilter(String bruh) {
        //Заполняем лист подходящими значениями
        someList.clear();
        for (int i = 0; i < organizationList.size(); i++) {
            if (organizationList.get(i).getStatus().equals(bruh)) {
                someList.add(organizationList.get(i));
            }
        }
        //Вызов кастомного метода
        someListt();
    }

    //При нажатии на элемент фильтра имён
    public static void findHope(String find) {
        //Заполняем лист подходящими значениями
        someList.clear();
        for (int i = 0; i < organizationList.size(); i++) {
            if (organizationList.get(i).getHope().contains(find)) {
                someList.add(organizationList.get(i));
            }
        }
        //Вызов кастомного метода
        someListt();
    }

    //При нажатии на элемент фильтра описаний
    public static void findDiscr(String find) {
        //Заполняем лист подходящими значениями
        someList.clear();
        for (int i = 0; i < organizationList.size(); i++) {
            if (organizationList.get(i).getDiscription().contains(find)) {
                someList.add(organizationList.get(i));
            }
        }
        //Вызов кастомного метода
        someListt();
    }

    //Кастомный метод заполнения значений в RecView
    static private void someListt () {
        sometext.setVisibility(View.INVISIBLE);
        if (!someList.isEmpty()) {
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new HopesAdapter(someList);
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
            sometext.setVisibility(View.VISIBLE);
        }
    }

    //При нажатии на Back (спасибо Артемию)
    @Override
    public void onBackPressed() {
        if(!back_presed){
            back_presed = true;
            Toast.makeText(this, "Нажмите ещё раз что-бы выйти", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    back_presed = false;
                }
            },2000);
        }else{
            System.exit(0);
        }
    }
}
