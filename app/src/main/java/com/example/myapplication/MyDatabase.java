package com.example.myapplication;

import com.raizlabs.android.dbflow.annotation.Database;

//Класс опимания базы данных
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME =  "MyDatabaseDB";
    public static final int VERSION = 1;
}
