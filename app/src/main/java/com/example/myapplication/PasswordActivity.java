package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordActivity extends AppCompatActivity {

    SharedPreferences sh;
    static SharedPreferences.Editor ed;
    LinearLayout inv;
    static EditText password;
    EditText password2;
    boolean wat = false;
    Button yes;
    Animation anim;
    FragmentManager manager;
    boolean back_presed = false;
    static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        //Настройка SharedPreferences
        sh = getSharedPreferences("SweetHopes", 0);
        ed = sh.edit();

        manager = getSupportFragmentManager();
        context = this;

        //Задание айдишек
        inv = findViewById(R.id.inv);
        password = findViewById(R.id.password);
        password2 = findViewById(R.id.password2);
        yes = findViewById(R.id.yes);

        //Настройка
        inv.setVisibility(View.INVISIBLE);
    }

    //При положительном ответе
    public void onYesClicked (View v) {
        //Проверка на то, показались ли внизу поля для ввода пароля
        if (!wat) {
            //Вызов анимации
            inv.setVisibility(View.VISIBLE);
            anim = AnimationUtils.loadAnimation(this, R.anim.textview_anim);
            anim.setDuration(750);
            inv.setAnimation(anim);
            yes.setText("Продолжить");
            wat = true;
        } else {
            //Проверка на пустые поля
            if (!TextUtils.isEmpty(password.getText()) && !TextUtils.isEmpty(password2.getText())) {
                //Проверка на совпадение паролей
                String str1 = String.valueOf(password.getText()), str2 = String.valueOf(password2.getText());
                if (str1.equals(str2)) {
                    //Проверка на "пробел"
                    if (!str1.equals(" ")) {
                        ed.putBoolean("ispassword", true);
                        ed.putString("password", String.valueOf(password.getText()));
                        ed.commit();

                        Intent intent = new Intent(PasswordActivity.this, LoginActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Пароль не может быть пробелом!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Пароли не совпадают!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Есть пустые поля!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onNoClicked (View v) {
        NoDialog dialog = new NoDialog();
        dialog.show(manager, "heh");
    }

    public static void goToNewAct() {
        ed.putBoolean("ispassword", true);
        ed.putString("password", " ");
        ed.commit();
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    //При нажатии на Back (спасибо Артемию)
    @Override
    public void onBackPressed() {
        if(!back_presed){
            back_presed = true;
            Toast.makeText(this, "Нажмите ещё раз что-бы выйти", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    back_presed = false;
                }
            },2000);
        }else{
            System.exit(0);
        }
    }
}
