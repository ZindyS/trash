package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

//Класс SpashScreen'а
public class SplashScreen extends AppCompatActivity {

    TextView editText;
    ImageView imageView;
    Animation text, image;
    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //Убираем верхнюю панельку с временем, зарядкой и пр.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Настраиваем SharedPreferences
        sh = getSharedPreferences("SweetHopes", 0);

        //Айдищки
        editText = findViewById(R.id.splashscreen_text);
        imageView = findViewById(R.id.splashscreen_image);

        //Настройка анимаций
        text = AnimationUtils.loadAnimation(this, R.anim.textview_anim);
        text.setDuration(1500);
        image = AnimationUtils.loadAnimation(this, R.anim.imageview_anim);
        image.setDuration(1500);

        //Задание анимаций
        editText.setAnimation(text);
        imageView.setAnimation(image);

        //Добавляем задержку
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Проверка на то, вводил ли пароль пользователь
                if (!sh.getBoolean("ispassword", false)) {
                    //Переход на след. активность
                    Intent intent = new Intent(SplashScreen.this, PasswordActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    if (sh.getString("password", "123").equals(" ")) {
                        //Переход на след. активность
                        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        //Переход на след. активность
                        Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }, 2500);
    }
}
